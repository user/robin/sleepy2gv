#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* ToDo:
 *  1) Smarter automatic eliding (at least understand why existing code doesn't
 *  work as we'd like)
 *  2) Directed eliding
 *    - Elide X and below
 *    - Elide everything below X
 *    - Show only calls below X
 *
 */

/* Example command line:
 *
 * ./sleepy2gv mupdf.slpy -c -o mupdf.dot -C -x seekfile readfile -u main -x FT_* jpeg_* inflate* inflate
 */

#define MAX_LINE 256
#define MAX_CALLDEPTH 256

enum {
    flag_elide_top = 1,
    flag_elide_bottom = 2
};



typedef struct symbol_s
{
    char *module;
    char *external;
    char *source;
    int   line;
    int   flags;
} symbol_t;

typedef struct callstack_s
{
    float t;
    int   depth;
    int   start;
    int   stop;
    int   sym[1];
} callstack_t;

typedef struct options_s
{
    char *indir;
    char *outfile;
    FILE *fout;

    int sym_max;
    symbol_t *sym_list;

    int call_count;
    int call_max;
    callstack_t **call_list;

    int verbose;
    int percents;
    int colorize;
    int clib;

    float  globaltotal;
    float *totals;
    float *callees;
    float *callers;
} options_t;

static void malloc_fail(void)
{
    fprintf(stderr, "Out of memory!\n");
    exit(EXIT_FAILURE);
}

static void parse_fail(void)
{
    fprintf(stderr, "Failed to parse input file!\n");
    exit(EXIT_FAILURE);
}

static void skip_ws(FILE *in)
{
    int c;

    do {
        c = fgetc(in);
        if (c < 0)
            return;
    } while (c <= 32);
    ungetc(c, in);
}


static char *read_quoted(FILE *in)
{
    char text[MAX_LINE];
    int i = 0;
    int c;

    if (fgetc(in) != '"')
        parse_fail();
    do {
        c = fgetc(in);
        if (c == '"')
            break;
        if (i < MAX_LINE-1)
            text[i++] = c;
    } while (i < MAX_LINE-1);
    text[i++] = 0;
    skip_ws(in);

    return strdup(text);
}

static int read_sym_detect_le(FILE *in, int *le)
{
    char text[MAX_LINE];
    int i = 0;
    int c;

    *le = 0;
    c = fgetc(in);
    if (c != 's')
        parse_fail();
    c = fgetc(in);
    if (c != 'y')
        parse_fail();
    c = fgetc(in);
    if (c != 'm')
        parse_fail();
    do {
        c = fgetc(in);
        if ((c < '0') || (c > '9')) {
            *le = ((c == 10) || (c == 13) || (c < 0));
            break;
        }
        if (i < MAX_LINE-1)
            text[i++] = c;
    } while (1);
    text[i++] = 0;
    skip_ws(in);

    return atoi(text);
}

static int read_sym(FILE *in)
{
    int le;

    return read_sym_detect_le(in, &le);
}

static int read_int(FILE *in)
{
    char text[MAX_LINE];
    int i = 0;
    int c;
    
    do {
        c = fgetc(in);
        if (c < '0' || c > '9')
            break;
        if (i < MAX_LINE-1)
            text[i++] = c;
    } while (i < MAX_LINE-1);
    text[i++] = 0;
    skip_ws(in);

    return atoi(text);
}

static float read_float(FILE *in)
{
    char text[MAX_LINE];
    int i = 0;
    int c;
    
    do {
        c = fgetc(in);
        if ((c < '0' || c > '9') && (c != '.') && (c != '+') && (c != '-'))
            break;
        if (i < MAX_LINE-1)
            text[i++] = c;
    } while (i < MAX_LINE-1);
    text[i++] = 0;
    skip_ws(in);

    return atof(text);
}

static void read_symbol(options_t *opts, FILE *in)
{
    int   internal = read_sym(in);
    char *module   = read_quoted(in);
    char *external = read_quoted(in);
    char *source   = read_quoted(in);
    int   line     = read_int(in);
    int   flags    = 0;

    if (internal >= opts->sym_max) {
        int new_size = internal+1;
        opts->sym_list = realloc(opts->sym_list,
                                 new_size * sizeof(*opts->sym_list));
        if (opts->sym_list == NULL)
            malloc_fail();
        memset(&opts->sym_list[opts->sym_max], 0,
               sizeof(*opts->sym_list) * (new_size-opts->sym_max));
        opts->sym_max = new_size;
    }

    /* FIXME: Should be a better test */
    if ((external[0] == '[') || (source[0] == '[') || (source[0] == 0)) {
        flags = flag_elide_top | flag_elide_bottom;
    }
    
    opts->sym_list[internal].module   = module;
    opts->sym_list[internal].external = external;
    opts->sym_list[internal].source   = source;
    opts->sym_list[internal].line     = line;
    opts->sym_list[internal].flags    = flags;
    if (opts->verbose >= 2) {
        fprintf(stderr, "sym%d \"%s\" \"%s\" \"%s\" %d %d\n",
                internal, module, external, source, line, flags);
    }
    
}    
    
static void read_symbols(options_t *opts, char *fname)
{
    FILE *in;

    in = fopen(fname, "r");
    if (in == NULL) {
        fprintf(stderr, "Failed to open '%s' for reading", fname);
        exit(EXIT_FAILURE);
    }
    if (opts->verbose >= 2)
        fprintf(stderr, "Symbols:\n");
    while (!feof(in)) {
        read_symbol(opts, in);
    }
    
    fclose(in);
}

static void read_callstack(options_t *opts, FILE *in)
{
    float        t = read_float(in);
    int          syms[MAX_CALLDEPTH];
    int          depth = 0;
    int          le, sym;
    callstack_t *call;
    
    do {
        sym = read_sym_detect_le(in, &le);
        if (depth < MAX_CALLDEPTH)
            syms[depth++] = sym;
    } while(le != 1);
    
    if (opts->call_count == opts->call_max) {
        opts->call_max *= 2;
        if (opts->call_max == 0)
            opts->call_max = 128;
        opts->call_list = realloc(opts->call_list,
                                  opts->call_max * sizeof(*opts->call_list));
        if (opts->call_list == NULL)
            malloc_fail();
    }
    call = malloc(sizeof(*call) + (depth-1)*sizeof(int));
    if (call == NULL)
        malloc_fail();
    call->t = t;
    call->start = 0;
    call->stop  = depth;
    call->depth = depth;
    memcpy(&call->sym[0], syms, sizeof(int) * depth);
    opts->call_list[opts->call_count++] = call;
    if (opts->verbose >= 2) {
        fprintf(stderr, "%g", call->t);
        for (depth=0; depth < call->depth; depth++) {
            char *e = "";
            sym = call->sym[depth];
            if (opts->sym_list[sym].external != NULL)
                e = opts->sym_list[sym].external;
            fprintf(stderr, " sym%d(%s)", sym, e);
        }
        fprintf(stderr, "\n");
    }
}    

static void read_callstacks(options_t *opts, char *fname)
{
    FILE *in;

    in = fopen(fname, "r");
    if (in == NULL) {
        fprintf(stderr, "Failed to open '%s' for reading", fname);
        exit(EXIT_FAILURE);
    }
    if (opts->verbose >= 2)
        fprintf(stderr, "Callstacks:\n");
    while (!feof(in)) {
        read_callstack(opts, in);
    }
    
    fclose(in);
}

static void read_sleepy(options_t *opts)
{
    FILE *in;
    char text[FILENAME_MAX];

    snprintf(text, FILENAME_MAX, "%s/Symbols.txt", opts->indir);
    if (opts->verbose)
        fprintf(stderr, "Reading '%s'\n", text);
    read_symbols(opts, text);

    snprintf(text, FILENAME_MAX, "%s/Callstacks.txt", opts->indir);
    if (opts->verbose)
        fprintf(stderr, "Reading '%s'\n", text);
    read_callstacks(opts, text);
}

static void parse_options(options_t *opts, int argc, char *argv[])
{
    int i = 1;

    while (i < argc) {
        if (argv[i][0]=='-') {
            switch (argv[i][1]) {
                case 'o':
                    if (opts->outfile != NULL)
                        goto fail;
                    opts->outfile=&argv[i][2];
                    if (argv[i][2] == 0) {
                        i++;
                        if (i >= argc)
                            goto fail;
                        opts->outfile=argv[i];
                    }
                    break;
                case 'v':
                    opts->verbose++;
                    break;
                case 'p':
                    opts->percents^=1;
                    break;
                case 'c':
                    opts->colorize^=1;
                    break;
                case 'C':
                    opts->clib^=1;
                    break;
                case 'u':
                    i++;
                    break;
                case 'x':
                case 'y':
                    if (argv[i][2] != 0)
                        goto fail;
                    while ((i+1 < argc) && (argv[i+1][0] != '-'))
                        i++;
                    break;
                default:
                    goto fail;
            }
        } else if (opts->indir == NULL) {
            opts->indir = argv[i];
        } else {
            goto fail;
        }
        i++;
    }
    if (opts->indir == NULL) {
      fail:
        fprintf(stderr, "Syntax: sleepy2gv <args> <indir>\n\n"
                "Converts <indir> to gv file. If no outfile given output"
                " on stdout.\n"
                "  args:\n"
                "    -c             colorize\n"
                "    -o outfile     specify outfile\n"
                "    -p             label edge percentages\n"
                "    -u sym1        only show calls under and including sym1\n"
                "    -v             verbose (more -v's = more verbose)\n"
                "    -x sym1 ...    only show calls above and including sym1 etc\n"
                "    -y sym1 ...    only show calls above sym1 etc\n"
                "    -C             hide common C lib functions\n"
                "\nsym1 etc can be of the form 'prefix*'.\n"
                );
        exit(EXIT_FAILURE);
    }
}

static void write_gv(options_t *opts)
{
    FILE *out;
    int   sym, call, depth, sym2;
    float total, globaltotal;
    int   lastprint;
    
    opts->callers = malloc(sizeof(*opts->callers) * opts->sym_max);
    if (opts->callers == NULL)
        malloc_fail();
    opts->callees = malloc(sizeof(*opts->callees) * opts->sym_max);
    if (opts->callees == NULL)
        malloc_fail();
    opts->totals = malloc(sizeof(*opts->totals) * opts->sym_max);
    if (opts->totals == NULL)
        malloc_fail();
    
    if (opts->outfile == NULL)
        out = stdout;
    else
        out = fopen(opts->outfile, "w");
    if (out == NULL) {
        fprintf(stderr, "Failed to open output file '%s'\n", opts->outfile);
        exit(EXIT_FAILURE);
    }

    if (opts->verbose >= 1)
        fprintf(stderr, "Collating: Pass 1\n");
    
    memset(opts->totals, 0, sizeof(*opts->totals) * opts->sym_max);

    globaltotal = 0.0;
    for (call = 0; call < opts->call_count; call++)
        globaltotal += opts->call_list[call]->t;
    opts->globaltotal = globaltotal;

    lastprint = 0;
    for (sym = 0; sym < opts->sym_max; sym++) {
        if (opts->sym_list[sym].external == NULL)
            continue;
        if ((opts->sym_list[sym].flags & (flag_elide_top | flag_elide_bottom)))
            continue;
        total = 0.0f;
        for (call = 0; call < opts->call_count; call++)
        {
            callstack_t *stack = opts->call_list[call];
            int start = stack->start;
            for (depth=stack->stop-1; depth >= start; depth--) {
                if (stack->sym[depth] == sym) {
                    total += stack->t;
                }
            }
        }
        if ((opts->verbose >= 1) && ((lastprint>>8) != (sym>>8))) {
            lastprint = sym;
            fprintf(stderr, " %2g%%", 100.0*sym/opts->sym_max);
        }
        opts->totals[sym] = total;
    }
        
    fprintf(out, "digraph sleepy {\n  layout=dot;shape=box;\n");

    for (sym = 0; sym < opts->sym_max; sym++) {
        if (opts->sym_list[sym].external == NULL)
            continue;
        if ((opts->sym_list[sym].flags & (flag_elide_top | flag_elide_bottom)))
            continue;
        if (opts->totals[sym] == 0.0)
            continue;
        fprintf(out, "  sym%d [label=\"%s\"", sym,
                opts->sym_list[sym].external);
        if (opts->colorize) {
            int g = 255-255*(opts->totals[sym]/globaltotal);
            fprintf(out, ",style=filled,fillcolor=\"#%2x%2x%2x\",fontcolor=%s",
                    g, g, g,
                    (g > 128 ? "black" : "white"));
        }
        fprintf(out, "];\n");
    }

    if (opts->verbose >= 1)
        fprintf(stderr, "\nCollating: Pass 2\n");
    lastprint = 0;
    for (sym = 0; sym < opts->sym_max; sym++) {
        if (opts->sym_list[sym].external == NULL)
            continue;
        if (opts->sym_list[sym].flags & (flag_elide_top | flag_elide_bottom))
            continue;
        if (opts->totals[sym] == 0.0)
            continue;
        memset(opts->callers, 0, sizeof(*opts->callers) * opts->sym_max);
        memset(opts->callees, 0, sizeof(*opts->callees) * opts->sym_max);
        
        /* Gather the information for this symbol from all the callstacks */
        for (call = 0; call < opts->call_count; call++)
        {
            callstack_t *stack = opts->call_list[call];
            int stop = stack->stop-1;
            int start = stack->start;
            for (depth=stop; depth >= start; depth--) {
                if (stack->sym[depth] == sym) {
                    if (depth > 0)
                        opts->callees[stack->sym[depth-1]] += stack->t;
                    if (depth < stop)
                        opts->callers[stack->sym[depth+1]] += stack->t;
                }
            }
        }

        /* Output the information - only for callers */
        for (sym2=0; sym2 < opts->sym_max; sym2++) {
            float caller_perc,called_perc;
            if (opts->sym_list[sym2].external == NULL)
                continue;
            if (opts->sym_list[sym2].flags &
                (flag_elide_top | flag_elide_bottom))
                continue;
            if (opts->callers[sym2] == 0.0)
                continue;
            caller_perc = 100.0*opts->callers[sym2]/opts->totals[sym2];
            called_perc = 100.0*opts->callers[sym2]/opts->totals[sym];
            fprintf(out, "  sym%d -> sym%d [", sym2, sym);
            if (opts->percents)
                fprintf(out, "taillabel=\"%.1f%%\",headlabel=\"%.1f%%\",",
                        caller_perc, called_perc);
            if (opts->colorize) {
                int t = ((unsigned)rand()) % 3;
                int r = ((t == 0) ? 0 : rand() & 255);
                int g = ((t == 1) ? 0 : rand() & 255);
                int b = ((t == 2) ? 0 : rand() & 255);
                fprintf(out, "color=\"#%2x%2x%2x\",", r, g, b);
            }
            
            fprintf(out, "weight=%d,", 1+(int)caller_perc);
#if 1
            fprintf(out, "penwidth=%g];\n", 0.5+caller_perc/10);
#else
            fprintf(out, "style=%s];\n",
                    (caller_perc > 70 ? "bold" :
                     (caller_perc > 30 ? "dashed" : "dotted")));
#endif
        }
        if ((opts->verbose >= 1) && ((lastprint>>8) != (sym>>8))) {
            lastprint = sym;
            fprintf(stderr, " %2g%%", 100.0*sym/opts->sym_max);
        }
    }
    fprintf(out, "}\n");
    if (out != stdout)
        fclose(out);
    if (opts->verbose >= 1)
        fprintf(stderr, "\n");
}

static void do_elide(options_t *opts)
{
    int call;
    int changed;
    int elided = 0;
    int depth;
    
    if (opts->verbose >= 1)
        fprintf(stderr, "Eliding\n");
    if (opts->verbose >= 2)
        fprintf(stderr, "  Keeping: ");
    do {
        changed = 0;
        for (call=0; call < opts->call_count; call++) {
            callstack_t *stack = opts->call_list[call];
            if (stack->depth > 1) {

#define TRY_KEEP(S,I,F,C)                                       \
 if (opts->sym_list[stack->sym[S]].flags & F) {                 \
     if (!(opts->sym_list[stack->sym[S+I]].flags & F)) {        \
         opts->sym_list[stack->sym[S]].flags &= ~F;             \
         C++;                                                   \
         if (opts->verbose >= 2) {                              \
             fprintf(stderr, "Keeping sym%d(%s) because of sym%d(%s)\n", \
                     stack->sym[S],                             \
                     opts->sym_list[stack->sym[S]].external,    \
                     stack->sym[S+I],                           \
                     opts->sym_list[stack->sym[S+I]].external); \
         }                                                      \
      }                                                         \
  }

                /* If a symbol has the 'elide_from_top' bit set, then we
                 * expect all callers to also have that bit set. If they
                 * don't, then remove the bit, and mark us as changed. */
                int sym = stack->sym[stack->depth-1];
                TRY_KEEP(stack->depth-1,-1,flag_elide_top,changed)
                /* If a symbol has the 'elide_from_bottom' bit set, then we
                 * expect all callees to also have that bit set. If they
                 * don't, then remove the bit, and mark us as changed. */
                sym = stack->sym[0];
                TRY_KEEP(0,1,flag_elide_bottom,changed)
            }
            for (depth=stack->depth-2; depth > 0; depth--) {
                /* If a symbol has the 'elide_from_top' bit set, then we
                 * expect all callers to also have that bit set. If they
                 * don't, then remove the bit, and mark us as changed. */
                TRY_KEEP(depth,-1,flag_elide_top,changed)
                /* If a symbol has the 'elide_from_bottom' bit set, then we
                 * expect all callees to also have that bit set. If they
                 * don't, then remove the bit, and mark us as changed. */
                TRY_KEEP(depth,+1,flag_elide_bottom,changed)
            }
        }
        elided += changed;
        if (opts->verbose >= 2)
            fprintf(stderr, " %d", elided);
    }
    while (changed);

    if (opts->verbose >= 2)
        fprintf(stderr, "\n");
    if (opts->verbose >= 3) {
        int sym;
        for (sym=0; sym < opts->sym_max; sym++) {
            if (opts->sym_list[sym].flags & (flag_elide_bottom | flag_elide_top)) {
                fprintf(stderr, "  removed(%d): %s\n", opts->sym_list[sym].flags, opts->sym_list[sym].external);
                
            }
        }
    }
}

static int my_strcmp(const char *c, const char *t) 
{
    while (*c == *t) {
        if (*c == 0)
            return (*t-*c);
        c++;
        t++;
    }
    if (*c == 0)
        return 1;
    if (*t == '*') {
        return 0;
    }
    
    return *t - *c;
}

static int find_sym(options_t *opts, char *name, int prev)
{
    int i;

    for (i=prev+1; i < opts->sym_max; i++) {
        if (my_strcmp(opts->sym_list[i].external, name) == 0)
            return i;
    }
    return -1;
}


static void do_lower_limit(options_t *opts, char *name, int off, int warn) 
{
    int sym = -1;
    int call;
    int depth;

    do {
        sym = find_sym(opts, name, sym);
        if (sym < 0) {
            if (warn)
                fprintf(stderr, "Failed to find symbol '%s'\n", name);
            return;
        }
        warn = 0;
        for (call=0; call < opts->call_count; call++) {
            callstack_t *stack = opts->call_list[call];
            int start = stack->start;
            int stop  = stack->stop;
            for (depth=stop-1; depth >= start; depth--) {
                if (stack->sym[depth] == sym) {
                    stack->start = depth + off;
                }
            }
        }
    } while (1);
}

static void do_upper_limit(options_t *opts, char *name)
{
    int sym = -1;
    int call;
    int depth;
    int warn = 1;

    do {
        sym = find_sym(opts, name, sym);
        if (sym < 0) {
            if (warn)
                fprintf(stderr, "Failed to find symbol '%s'\n", name);
            return;
        }
        warn = 0;
        for (call=0; call < opts->call_count; call++) {
            callstack_t *stack = opts->call_list[call];
            int start = stack->start;
            int stop  = stack->stop;
            for (depth=start; depth < stop; depth++) {
                if (stack->sym[depth] == sym) {
                    stack->stop = depth+1;
                }
            }
        }
    } while (1);
}

static void do_specified_limits(options_t *opts, int argc, char *argv[], int warn)
{
    int i;

    for (i=1; i < argc; i++) {
        if ((argv[i][0] == '-') && (argv[i][1] == 'x') && (argv[i][2] == 0)) {
            while ((i+1 < argc) && (argv[i+1][0] != '-')) {
                i++;
                do_lower_limit(opts, argv[i], 0, warn);
            }
        } else if ((argv[i][0] == '-') && (argv[i][1] == 'y') &&
                   (argv[i][2] == 0)) {
            while ((i+1 < argc) && (argv[i+1][0] != '-')) {
                i++;
                do_lower_limit(opts, argv[i], 1, warn);
            }
        } else if ((argv[i][0] == '-') && (argv[i][1] == 'u') &&
                   (argv[i][2] == 0)) {
            i++;
            do_upper_limit(opts, argv[i]);
        }
    }
}

/* This is a bit crap, but it works for me. */
static char *clib_elides[] = {
    "dummy",
    "-y", "memcpy", "memset", "memcmp", "strcpy", "strcmp", "strcat",
    "strstr", "strchr", "qsort", "bsearch", "atoi", "fprintf", "printf",
    "sprintf", "vsprintf", "vsnprintf", "snprintf",
    "fread", "fwrite", "fopen", "fclose", "malloc", "free", "realloc",
    "calloc", "getenv", "atoi", "atol", "atof", "__from_strstr_to_strchr",
    "_ftol2_pentium4", "_alldiv", "_allmul", "_allshr", "_strtoi64",
};

int main(int argc, char *argv[])
{
    options_t opts;

    memset(&opts, 0, sizeof(opts));
    parse_options(&opts, argc, argv);

    read_sleepy(&opts);

    //do_elide(&opts);
    do_specified_limits(&opts, argc, argv, 1);
    if (opts.clib)
        do_specified_limits(&opts, sizeof(clib_elides)/sizeof(*clib_elides),
                            clib_elides, 0);
    
    write_gv(&opts);
     
    return EXIT_SUCCESS;
}
