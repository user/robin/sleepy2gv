What is this?

 sleepy2gv is a simple tool for making 'nice' diagrams from the output of
 the free profiler 'Very Sleepy', available from:

  <http://www.codersnodes.com/sleepy>

 You will need the 'dot' tool from GraphViz installed to actually generate
 the diagrams.

Build instructions:

 gcc sleepy2gv.c -o sleepy2gc

Use instructions:

 1) Profile your code (let's say mupdf for example) using sleepy as usual.
    Save out the mupdf.sleepy file.

 2) mkdir mupdf.unpacked && cd mupdf.unpacked && unzip ../mupdf.sleepy && cd ..
    (Yes, I'm too lazy to have written code to read the zipped form of the
    data.)

 3) sleepy2gv -c -C mupdf.unpacked -o mupdf.dot -v

    This generates you a .dot file based on the profiled data.

    There are more command line options. See sleepy2gv -h for more
    information. A command line that I use for mupdf is as follows:
        ./sleepy2gv mupdf.unpacked -c -o mupdf.dot -C -x seekfile readfile
        -u main -x FT_* jpeg_* inflate* inflate -y fz_malloc fz_free fz_calloc
        fz_resolveindirect fz_mul255 fz_dictgets
     This shows only stuff under main and hides everything in the C lib, or in
     commonly called utility functions.

  4) dot -Tpng mupdf.dot -omupdf.png -v

     This generates the actual png output. See the graphviz docs for other
     supported formats.
